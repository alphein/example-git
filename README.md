# simples exemplo de uso do git

alguns comandos básicos:

git init: inicia o repositório local do git num diretório
git add: adiciona um arquivo
git commit: registra alterações
git log: vê o log de registros
git diff: vê as diferenças entre cada estado (commits, branchs, tags, etc.)
git branch: gerencia as ramificações do código
git tag: faz "marcações" normalmente usado para marcar releases
git checkout: permite "voltar no tempo"
git remote: administra as informações sobre repositórios remotos
git push: envia as alterações ao repositório remoto
git clone: clona de um repositório remoto criando um local
git fetch: baixa as alterações feitas num repositório remoto
git pull: aplica as alterações baixadas de um repositório remoto


asciinema parcial desse repo:

[![asciicast](https://asciinema.org/a/SP30mw6hJ07NvGvH7Zv2Z5HAl.png)](https://asciinema.org/a/SP30mw6hJ07NvGvH7Zv2Z5HAl)
